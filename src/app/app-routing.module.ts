import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingBagComponent } from '././shopping-bag/shopping-bag.component'
import { ProductListComponent } from '././product-list/product-list.component'
import { ProductDetailsComponent } from './product-details/product-details.component';

const routes: Routes = [
  { path:'', component: ProductListComponent },
  { path: 'products/:productId', component: ProductDetailsComponent },
  { path:'shoppingbag', component: ShoppingBagComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
